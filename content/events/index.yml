title: Events
description: Where you'll find information on the GitLab happenings circuit
header:
  title: GitLab Events
  centered_by_default: true
  text: GitLab is The DevOps Platform that empowers organizations to maximize the overall return on software development. Join an event to learn how your team can deliver software faster and efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events this year, and cannot wait to see you there!
event_videos:
  header: Event Videos
  videos:
    - title: "Commit Virtual - Four Ways to Further FOSS"
      photourl: /nuxt-images/events/1.jpg
      video_link: https://www.youtube.com/embed/8h95PjdUyn4
      carousel_identifier:
        - 'Event Videos'

    - title: "How Delta became truly cloud native"
      photourl: /nuxt-images/events/2.jpg
      video_link: https://www.youtube-nocookie.com/embed/zV_hFcxoN8I
      carousel_identifier:
        - 'Event Videos'

    - title: "The Power of GitLab"
      photourl: /nuxt-images/events/3.jpg
      video_link: https://www.youtube-nocookie.com/embed/tIm643kyQqs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevOps Culture at Porsche - A GitLab success story"
      photourl: /nuxt-images/events/4.jpg
      video_link: https://www.youtube-nocookie.com/embed/O9MdFhaosRo
      carousel_identifier:
        - 'Event Videos'

    - title: "Creating a CI/CD Pipeline with GitLab and Kubernetes in 20 Minutes"
      photourl: /nuxt-images/events/5.jpg
      video_link: https://www.youtube-nocookie.com/embed/-shvwiBwFVI
      carousel_identifier:
        - 'Event Videos'

    - title: "GitLab Product Keynote at Commit SF"
      photourl: /nuxt-images/events/6.jpg
      video_link: https://www.youtube-nocookie.com/embed/lFIk7E38Djs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevSecOps & GitLab's Security Solutions"
      photourl: /nuxt-images/events/7.jpg
      video_link: https://www.youtube-nocookie.com/embed/CjX1TsCZgoQ
      carousel_identifier:
        - 'Event Videos'

    - title: "Gitlab Connect Paris 2019"
      photourl: /nuxt-images/events/8.jpg
      video_link: https://www.youtube-nocookie.com/embed/YwzpNNSdx_I
      carousel_identifier:
        - 'Event Videos'

    - title: "Verizon Connect achieves datacenter deploys in under 8 hours with GitLab"
      photourl: /nuxt-images/events/9.jpg
      video_link: https://www.youtube-nocookie.com/embed/zxMFaw5j6Zs
      carousel_identifier:
        - 'Event Videos'

    - title: "DataOps in a Cloud Native World"
      photourl: /nuxt-images/events/10.jpg
      video_link: https://www.youtube-nocookie.com/embed/PLe9sovhtGA
      carousel_identifier:
        - 'Event Videos'

events:
  - topic: Forrester study shows GitLab enables 427% ROI
    type: Webcast
    date_starts: February 7, 2023
    date_ends: February 7, 2023
    description: Join this webcast as we discuss with our guest Forrester the value of using GitLab Ultimate.
    location: Virtual
    region: EMEA, AMER
    social_tags:
    event_url: https://page.gitlab.com/webcast-forrester-tei-gitlab-ultimate.html
  - topic: Tech Demo - The DevOps Platform Deep Dive
    type: Webcast
    date_starts: January 25, 2023
    date_ends: January 25, 2023
    description: Join this live 30 minute technical demo to learn how simple it is to create, test, and deploy an application using the GitLab DevOps Platform and Kubernetes.
    location: Virtual
    region: EMEA, AMER
    social_tags:
    event_url: https://page.gitlab.com/devsecops-platform-tech-demo.html
  - topic: GitLab’s DevSecOps Innovations and Predictions for 2023
    type: Webcast
    date_starts: January 31, 2023
    date_ends: January 31, 2023
    description: Learn about GitLab's innovations from the latest quarter, predictions, and plans for 2023.
    location: Virtual
    region: AMER, EMEA
    social_tags:
    event_url: https://page.gitlab.com/webcast-gitlab-devsecops-innovations-predictions-2023.html
  - topic: DevOps Pro Europe 2023
    type: Conference
    date_starts: May 25, 2023 
    date_ends: May 26, 2023
    description: From the GitLab Data Team member, a first-face story about how to do the magic with data in one of the biggest all-remote companies in the world. As a person been trapped in the software industry and Data world for more than 18 years, he brings the experience learned from his own mistakes and answers the questions - why it is important to advocate for Open Source, how to embrace the DevOps culture, why transparency is a vital part of your DNA, what is the secret sauce of successful Data Teams
    location: Vilnius, Lithuania
    region: EMEA
    social_tags: 
    event_url: https://devopspro.lt/
  - topic: Big Data Belgrade
    type: MeetUp
    date_starts: January 26, 2023 
    date_ends: January 26, 2023
    description: After the holidays, the time has come for the second Big Data Belgrade meetup! The event will be held on Thursday, January 26, starting at 7pm at ICT Hub, Kralja Milana 10, Belgrade. GitLab Data team member will tell us about the data infrastructure that they built for success.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
    event_url: https://www.meetup.com/big-data-belgrade/events/290867093/
  - topic: Tech Demo - The DevOps Platform APAC
    type: Webcast
    date_starts: January 18, 2023
    date_ends: January 18, 2023
    description: Join this live 45 minute technical demo to learn how simple it is to create, test, and deploy an application using the GitLab DevOps Platform and Kubernetes.
    location: Virtual
    region: APAC
    social_tags:
    event_url: https://page.gitlab.com/devops-platform-demo-apac.html
  - topic: AWS CDK, Serverless Next.js, and GitLab - Minimal Maintenance, Maximum Impact
    type: Webcast
    date_starts: January 17, 2023
    date_ends: January 17, 2023
    description: What are you doing to make the most out of your pipelines? Join BoxBoat, GitLab's first Certified Professional Services Partner, in this webinar where we will be creating an advanced pipeline that runs full end-to-end tests, automatic updates, and deploys review apps! BoxBoat will also be showcasing a serverless implementation of Next.js’ framework deployed using AWS CDK. Attendees will also see some easy tips and tricks to speed up your pipelines and work around flaky processes.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/boxboatwebinar-registration-page.html

  - topic: Vueday 2022
    type: Conference
    date_starts: November 18, 2022
    date_ends: November 18, 2022
    description: Anna Vovchenko (she/her), Senior Frontend Engineer, will be giving a talk on reusable components. She will cover how the reusable components help to maintain clean code efficiently. This talk is a beginner-friendly guide on how to create and use a reusable component, what is its benefits and best practices.
    location: Verona, Italy and virtual
    region: EMEA
    event_url: https://2022.vueday.it/

  - topic: Kubernetes Community Days France 2023  
    type: Conference
    date_starts: March 7, 2023
    date_ends: March 7, 2023
    description: Visit GitLab at booth D2 at Kubernetes Community Days in Paris on March 7, 2023. The tech community will gather at the Center Pompidou located in the heart of Paris for a day of conferences on Cloud Native and DevOps technologies.
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://www.kcdfrance.fr/

  - topic: Milan Leader Summit
    type: Conference
    date_starts: March 9, 2023
    date_ends: March 9, 2023
    description: Join GitLab at ERTL-YANG Leader Summit in Milan on March 9, 2023. This summit is designed for business executives across industries to discuss and share insights on key business issues and to grow and deepen their professional networks.
    location: Milan, Italy
    region: EMEA
    social_tags:
    event_url: https://www.ertl-yang.com/summits-2023/ertl-yang-leader-milan/overview

  - topic: Connect Day Paris
    type: Conference
    date_starts: March 16, 2023
    date_ends: March 16, 2023
    description: We are pleased to invite you the first 100% GitLab event in France! It will be a unique opportunity to discover the most popular DevSecOps platform and to understand how we can help your organization to accelerate its activities. During this in-person event you will meet our experts, watch live demoos, connect with your peers and discover the success stories of our most successful customers and partners in France. Register today to reserve your spot!
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/connectdayparis-registration-page.html

  - topic: Tech Talk - GitLab for Governance and Compliance
    type: Webcast
    date_starts: December 6, 2022
    date_ends: December 6, 2022
    description: GitLab, The DevOps platform for software innovation provides your agency with one interface, one data store, one value stream, one spot to secure your code and a place for everyone to contribute. The platform is the only true cloud-agnostic end-to-end DevOps platform that brings together all DevOps capabilities in one place. [Join GitLab experts](https://carahevents.carahsoft.com/Event/Register/329405-gitlab?auth=13d1fe8b59814ca79a446c18076e6e4d) online to explore and discuss the challenges of building governance and compliance within your agency.
    location: Virtual
    region: PubSec
    social_tags:
    event_url: https://carahevents.carahsoft.com/Event/Details/329405-gitlab

  - topic: Deploy, Manage and Scale Kubernetes to AWS EKS with GitLab
    type: MeetUp
    date_starts: February 28, 2023
    date_ends: February 28, 2023
    description: Take your DevOps and Kubernetes skills to the next level. Join industry-leading experts from AWS and GitLab for a hands-on workshop that will enable you to deploy secure applications to Amazon EKS in record time. In this 4-hour hands-on workshop you will learn to use the GitLab Kubernetes Agent, an active in-cluster component for solving GitLab and Kubernetes integration tasks, while adhering to GitOps best practices. This event will be hands-on and is intended for builders. If you are interested in understanding and learning how GitLab accelerates development to Amazon EKS, this is for you.
    location: Zurich, Switzerland
    region: EMEA
    social_tags:
    event_url: https://deploymanageandscalekubernetes.splashthat.com/

  - topic: GBI CISO Exec Gathering Seattle
    type: Conference
    date_starts: February 23, 2023
    date_ends: February 23, 2023
    description: GitLab is a proud sponsor of the GBI Impact CISO Evening Gathering event in Seattle on February 23, 2023. This is an intimate networking event that will bring together the IT community to create a world-class, executive driven academic platform to help you make the right decisions for your organization. This gathering will discuss current top trends and how  professionals will utilise technology, process, and leadership to gain a competitive advantage. The gathering features over 20 professionals creating a dynamic, agile mix of thought leadership and best practice strategies. This is an invite-only event so register to request an invite. If attending, we look forward to meeting you there.
    location: Seattle, Washington
    region: AMER
    social_tags:
    event_url: https://www.gbiimpact.com/evening-gathering-registration-ciso-seattle-evening-gathering-february-23rd

  - topic: GitLab Security + Compliance Workshop
    type: Webcast
    date_starts: January 12, 2023
    date_ends: January 12, 2023
    description: Cyber attacks have never been more in the news. From Twitter hacks to identity theft, vulnerabilities are exposing gaps in the application development process. Application security is difficult, especially when security is a separate process from your DevOps workflow. Security has traditionally been the final hurdle to conquer in the development lifecycle. Join this three hour hands-on workshop to gain a better understanding of how to successfully shift security left to find and fix security flaws during development - and to do so more easily and with greater visibility and control than typical approaches can provide.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/amersecurity-ws-virtual-registration-page.html

  - topic: 2022 Accelerate State of DevOps - Report Roundup
    type: Webcast
    date_starts: November 16, 2022
    date_ends: November 16, 2022
    description: In this 8th year of the Accelerate State of DevOps report from Google Cloud’s DevOps Research and Assessment (DORA) team, there is renewed focus on securing the software supply chain and understanding how it drives overall organizational performance. Join this roundtable discussion to explore how security is deeply coupled with DevOps adoption, and how it drives delivery and organizational performance amongst organizations.
    location: Virtual
    region: AMER, EMEA
    social_tags:
      - DevOps
    event_url: https://page.gitlab.com/accelerate-state-of-devops-webcast.html

  - topic: DORA Metrics with GitLab
    type: Webcast
    date_starts: November 15, 2022
    date_ends: November 15, 2022
    description: Join this technical demo to learn how GitLab supports, implements and reports DORA metrics to provide insight into the effectiveness of an organization’s development and delivery practices.
    location: Virtual
    region: AMER, EMEA
    social_tags:
      - DevOps
    event_url: https://page.gitlab.com/dora-metrics-tech-demo-lp.html

  - topic: DevSecOps Governance Framework Webinar - How to Enhance & Automate Compliance
    type: Webcast
    date_starts: January 25, 2023
    date_ends: January 25, 2023
    description: Implementing and enforcing compliance standards consistently can be difficult and time-consuming for even the most advanced DevSecOps teams. Join us to learn from GitLab’s professional services experts, who help enterprises like yours adopt a DevSecOps framework to leverage built-in compliance and security.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/devsecopwc-registration-page.html

  - topic: GitLab DevOps Automation Virtual Workshop
    type: Webcast
    date_starts: December 15, 2022
    date_ends: December 15, 2022
    description: Uncover how the most effective Application Development teams leverage GitLab to automate their DevOps process and achieve material business outcomes. In this 3-hour simulated team environment, attendees will respond to various scenarios utilizing GitLab's single application for the entire DevOps lifecycle. No previous experience with GitLab is required.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/devopsautws-virtual-registration-page.html

  - topic: Advanced CI/CD Virtual Workshop
    type: Webcast
    date_starts: January 24, 2023
    date_ends: January 24, 2023
    description: In this 3-hour simulated team environment, attendees will dive into GitLab Advanced CI/CD features guided by our Solution Architects. This class is for experienced CI users that want to learn advanced GitLab features
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/advancecicdws-registration-landing-page.html

  - topic: AWS re:Invent 2022
    type: Conference
    date_starts: November 28, 2022
    date_ends: December 2, 2022
    description: Stop by the GitLab Booth 228 and talk to one of our many DevOps experts and see The One DevOps Platform in action! Dive into new capabilities, learn best practices, and get answers to all of your technical questions. Share your feedback and let us know what you'd like to see in the GitLab platform! And that's not all! We'll have an exciting lineup of action-packed lightning talks happening every hour in the booth. Using GitLab's unique platform capabilities, we will show you how Nasdaq is building onboarding templates to provide provisioning, deployment, release and security best practices for day 1 activities. You'll walk away from this breakout session happening on Thursday, December 1 at 11:45 AM at the MGM in room 115 with knowledge about how highly regulated companies implement SplatOps techniques for auditing and compliance. Lastly, come to our DevOps & Darts event to throw darts, talk DevOps, and enjoy tasty food and drink while DJ Graffiti spins. The happy hour will be at Flight Club in the Venetian Grand Canal Shoppes. Check it out the details and register to attend our happy hour [here](https://about.gitlab.com/events/aws-reinvent/)!
    location: Las Vegas, NV
    region: Amer
    social_tags:
      - awsreinvent
    event_url: https://about.gitlab.com/events/aws-reinvent/

  - topic: Planet Cyber Sec Long Beach
    type: Conference
    date_starts: December 6, 2022
    date_ends: December 6, 2022
    description: Come join us for a deep-dive into ‘Zero Trust’. We have invited Subject Matter Experts to cut through the hype and get down to a real discussion on this overused term. This event will bring together leaders in Information Security and Information Systems within government and private industry for a day of collaboration, networking and presentations by leading Information Security professionals. Hear from world-renown keynote speakers on important relevant topics and be better prepared to move forward on your ‘Zero Trust’ and other initiatives.Enhance your professional network by making new connections and catching up with old friends. Peer collaboration has never been more important than today. Gain new perspectives from peers who have experience with your critical priorities and are facing the same challenges – generate ideas, share real-life experiences and get forward-looking guidance and insights. Exchange best practices, validate strategies and walk away with practical insights that are immediately actionable.
    location: Long Beach, CA
    region: AMER
    social_tags:
    event_url: https://planetcybersec.com/120622-conference/

  - topic: GitHub to GitLab Migration Hands-On Virtual Workshop
    type: Webcast
    date_starts: November 15, 2022
    date_ends: November 15, 2022
    description: The GitHub to GitLab Migration workshop shows you the immediate value you can get out of migrating your projects to GitLab in only a few short hours. It goes beyond just migrating the project and highlights GitLab’s capabilities in the Security and CI/CD space.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/ghworkshop-virtual-registration-landing-page.html

  - topic: Bit2Bit Conference Peru
    type: Conference
    date_starts: December 6, 2022
    date_ends: December 6, 2022
    description: We invite you to our GitLab DevOps Experience event in which we will navigate through the DevOps cycle of our GitLab partner in Lima.
    location: Lima, Peru
    region: AMER
    social_tags:
    event_url: https://www.eventbrite.com/e/gitlab-devops-experience-lima-tickets-457083498047

  - topic: GitLab’s POV on the Current State of DevOps and Where it’s Headed
    type: Webcast
    date_starts: November 9, 2022
    date_ends: November 9, 2022
    description: Since its inception, DevOps has taken on multiple forms and definitions but kept the same goal - deliver value faster. As elusive as it has been to define, it continues to grow in popularity and pursuit of adoption. In this webinar, GitLab and NextLinkLabs have partnered up to discuss the historical, current and future contexts of DevOps while helping unpack how GitLab’s DevOps Platform helps companies deliver on their DevOps goals.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/premultweb-registration-page.html

  - topic: Advanced CI/CD Workshop for the Public Sector
    type: Webcast
    date_starts: November 10, 2022
    date_ends: November 10, 2022
    description: GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. You might have had the chance to take it for a test drive, but even our most advanced users know there’s still a lot to learn to push better and faster automation throughout your DevOps lifecycle! We’re inviting you to a deep-dive workshop on Advanced GitLab CI/CD, including everything you need to enable you to take your automation game to the next level, and provide thought leadership within your organization. Register for this complimentary workshop today!
    location: Virtual
    region: PubSec
    social_tags:
    event_url: https://page.gitlab.com/cicdworkshop-virtual-registration-page.html

  - topic: GitLab Connect - Customer Stories + TopGolf in the Silicon Valley
    type: Conference
    date_starts: November 9, 2022
    date_ends: November 9, 2022
    description: Join us to learn how Silicon Valley Bank is modernizing DevSecOps with large scale digital transformation goals to become a leading financial services innovator. You’ll hear from Prasanna Sivakumar, Head of DevOps Center for Excellence at Silicon Valley Bank, as he shares his teams’ two year journey to improve speed to market, employee productivity, operational resiliency, and cost savings. We’ll also have a thought leadership customer panel led by Lee Faus, GitLab Global Field CTO. The panel will share stories, DevSecOps best practices, and provide a collaborative discussion on how companies are establishing a new developer experience through approaches like platform engineering. We hope you’ll join us for this unique and fun event, where we can all learn from each other!
    location: Santa Clara, CA
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/gitlabconnectsantaclara-inperson-page.html

  - topic: Conf42 DevSecOps 2022
    type: Conference
    date_starts: December 1, 2022
    date_ends: December 1, 2022
    description: Come join GitLab at Conf42, a virtual confernce that will bring 3000+ tech communities EMEA wide together to discuss Secuirty in DevOps.
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://www.conf42.com/devsecops2022


  - topic: Big Data Conference Europe 2022
    type: Conference
    date_starts: November 23, 2022
    date_ends: November 24, 2022
    description: Big Data Conference Europe is a two-day conference with technical talks in the fields of Big Data, High Load, Data Science, Machine Learning and AI. Conference brings together developers, IT professionals and users to share their experience, discuss best practices, describe use cases and business applications related to their successes. Our Data team member will give an overview of how our **Data team** does the thing and how they use our DevOps product in their daily work to achieve exceptional results in a fast-paced environment. You will hear interesting story of the process of creating, improving, and scaling the Data product using a modern DevOps stack. Exposing details of the use case how we embrace the open-source philosophy to help us provide faster time to market. Will have a chance to discuss how to use the advantage of the internal product to make us more agile in the daily job of creating great data products.
    location: Virtual
    region: EMEA
    social_tags:
      - bigdataconference
    event_url: https://bigdataconference.eu/

  - topic: Innovate NZ
    type: Conference
    date_starts: November 10, 2022
    date_ends: November 10, 2022
    description: GitLab is looking forward to connecting with the Innovate NZ attendees at this event. Our booth number has not been given out at this time, but check back closer to time for more information!
    location: Wellington, NZ
    region: APAC
    social_tags:
    event_url: https://publicsectornetwork.co/event/innovate-nz-2022/

  - topic: Cloud Expo Paris
    type: Conference
    date_starts: November 16, 2022
    date_ends: November 17, 2022
    description: Join GitLab at the largest business show in France dedicated to the Cloud, Cyber ​​Security, DevOps, Big Data & IA and new technologies. Visit GitLab at Booth F70
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://www.cloudexpoeurope.fr/

  - topic: Digital NSW
    type: Conference
    date_starts: November 30, 2022
    date_ends: November 30, 2022
    description: GitLab is looking forward to connecting with the Australian Government attendees at this event. Our booth number has not been given out at this time, but check back closer to time for more information!
    location: Sydney, NSW
    region: APAC
    social_tags:
    event_url: https://publicsectornetwork.co/event/digital_nsw_2022/

  - topic: BeerOps Melbourne MeetUp
    type: MeetUp
    date_starts: November 29, 2022
    date_ends: November 29, 2022
    description: Join GitLab in Melbourne for the last BeerOps MeetUp of the year! - Australia's Largest DevOps & Data Meetup!
    location: Melbourne, VIC
    region: APAC
    social_tags:
    event_url: https://www.eventbrite.com.au/e/beerops-melb-australias-largest-devops-data-meetup-tickets-441427741257

  - topic: BeerOps Sydney MeetUp
    type: MeetUp
    date_starts: December 6, 2022
    date_ends: December 6, 2022
    description: Join GitLab in Sydney for the last BeerOps MeetUp of the year! - Australia's Largest DevOps & Data Meetup!
    location: Sydney, NSW
    region: APAC
    social_tags:
    event_url: https://www.eventbrite.com.au/e/beerops-syd-australias-largest-devops-data-meetup-tickets-441411382327

  - topic: Tech Rocks Summit Paris 2022
    type: Conference
    date_starts: December 8, 2022
    date_ends: December 9, 2022
    description: Join GitLab at the Tech.Rocks Summit which is back as a hybrid event! Come and enjoy the event for tech leaders and network with your peers.
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://www.tech.rocks/

  - topic: EMERGE 2022 - Forum on the Future of AI Driven Humanity & International Conference Digital Society Now
    type: Conference
    date_starts: December 16, 2022
    date_ends: December 18, 2022
    description: EMERGE is an annual event organised by the Digital Society Lab of the Institute for Philosophy and Social Theory, University of Belgrade. Its goal is to connect actors from the tech industry, policy makers, and academic researchers in discussing the social and economic impact of emerging technologies. EMERGE 2022 will consist of the EMERGE Forum on The Future of AI driven Humanity and the International Scientific Conference on Digital Society Now. How GitLab Data team embraces DevOps culture to move fast in a rapidly growing environment.Why you should find solutions for your challenges in the Open Source world. What are the vital points you should stick with in order to provide trusted data on time for your users. Providing a walkthrough over the process of creating, improving, and scaling the Data product using a modern DevOps stack in the Open Source world. Exposing details of the use case and how we embrace the open-source philosophy to help us provide faster time to market. We will discuss how to use the advantage of the internal product to make us more agile in the daily job of creating great data products.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
      - emerge2022
    event_url: https://emerge.ifdt.bg.ac.rs/

  - topic: Ruby on Rails Global Summit 2023 - How GitLab hires Ruby on Rails Engineers
    type: Conference
    date_starts: January 24, 2023
    date_ends: January 25, 2023
    description: This talk covers how GitLab hires Ruby on Rails engineers including sourcing candidates, choosing which to screen, technical evaluation, behavioral evaluation, reference check, background check, and offer. It should be enlightening for both developers and for hiring managers.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://events.geekle.us/ruby/

  - topic: Canterbury Hacker Camp 2022 - "Documentation-powered security"
    type: Conference
    date_starts: November 24, 2022
    date_ends: November 26, 2022
    description: Nick from GitLab's Application Security team will be sharing how GitLab's handbook-first approach drives success within the security team, and how organisations can move from dusty policies and procedures to lovable, usable documentation.
    location: Hamner Springs, Aotearoa
    region: APAC
    event_url: https://2022.chcon.nz/

  - topic: GitLab Contributor Days 2023.1
    type: MeetUp
    date_starts: February 7, 2023
    date_ends: February 8, 2023
    description: Attendees will have the chance to learn how to get started as a contributor, improve their skills, and learn best practices, tips, and tricks for contributing to open source and GitLab. There will be social activities in the city center of Ghent to strengthen connections among the community members in attendance.<br><br>GitLab Contributor Days is an unconference-style event, geared toward collaboration. There will be space to work on contributions and engage with your peers and GitLab team members. Mentors will be on hand to help answer your questions and work with you on your contributions.<br><br>The event is supported by GitLab, the open-core DevSecOps platform, and being run alongside Config Management Camp.
    location: Ghent, Belgium
    region: EMEA
    event_url: https://about.gitlab.com/community/contribute/development/
