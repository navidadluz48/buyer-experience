---
  title: "GitLab Commit: Take Your DevOps Results to the Next Level"
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  image_title: /nuxt-images/open-graph/commit2022_opengraph.png
  image_alt: GitLab Commit 2022 - Level Up Your DevOps Results
  twitter_image: /nuxt-images/open-graph/commit2022_opengraph.png

  hero:
    subtitle: Take your DevOps results to the next level
    text: |
      Get fresh ideas to fuel software innovation, embrace collaboration, and strengthen security at your organization. Check out the videos from previous keynotes and sessions at GitLab Commit.
  components:
    ## Add an 'anchor_id' value that matches corresponding navigation link above if section should be linkable
    - name: 'Carousel'
      data:
        header: Watch previous Commits
        videos:
          - title: "GitLab and UBS - Driving Innovation in the Financial Industry"
            photourl: /nuxt-images/events/gitlab-ubs-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/Tof-7fDultw
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "GitLab Secure & Protect: Bringing DevSecOps to Life"
            photourl: /nuxt-images/events/devsecops-to-life-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/InzXgpDuHJM
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "Innovation in Education"
            photourl: /nuxt-images/events/innovation-education-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/zRoQMrcxVB8
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "Scaling Collaboration for Remote Teams"
            photourl: /nuxt-images/events/remote-teams-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/8b9nWY45bEw
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "From Waterfall to Agile to DevOps with GitLab"
            photourl: /nuxt-images/events/waterfall-devops-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/5FwL9nZSFn0
            carousel_identifier:
              - 'Commit 2021 Playlist'

    - name: 'cta-box'
      data:
        anchor_id: speakers
        icon: speaker-presentation
        icon_color: transparent
        icon_size: lg
        background_color: '#5D5984'
        header: Interested in being a speaker?
        text: Interested in being a speaker? If you have an idea for a session related to DevOps, security, or application development, we'd love to hear it! We have a rolling call for proposals (CFP) for future GitLab events.
        button_text: Submit your talk
        button_url: https://docs.google.com/forms/d/e/1FAIpQLSdFkBUNc7r47IEyuKcVbzYT26h8PDm7Av5yzqOVE_9OrZLiwA/viewform?usp=send_form
        data_ga_name: submit your talk
        data_ga_location: commit registration body


    - name: 'content-block'
      data:
        header: Code of Conduct
        text: |
          GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](https://about.gitlab.com/company/culture/ecoc/){data-ga-name="code of conduct" data-ga-location="body"} to ensure Commit is a friendly, inclusive, and comfortable environment for all participants.
